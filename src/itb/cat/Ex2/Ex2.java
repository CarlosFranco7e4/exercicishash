package itb.cat.Ex2;

import java.io.File;
import java.nio.file.Files;
import java.security.MessageDigest;

public class Ex2 {
    public static void main(String[] args) {
        File arxiuText = new File("Ex2arx/adeu.txt");
        File arxiuText2 = new File("Ex2arx/hola.txt");
        File arxiuText3 = new File("Ex2arx/bonatarda.txt");
        hash(arxiuText);
        hash(arxiuText2);
        hash(arxiuText3);
        //Li he passat el hash de l'arxiu adeu, i provant, ho ha descobrit
    }

    public static void hash(File imatge){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA3-256");
            byte[] hashInBytes = new byte[0];
            hashInBytes = md.digest(Files.readAllBytes(imatge.toPath()));
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println(sb);
        } catch (Exception e) {
            e.printStackTrace(); }
    }
}
