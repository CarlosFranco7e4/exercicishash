package itb.cat.Ex3;

import java.io.File;
import java.nio.file.Files;
import java.security.MessageDigest;

public class Ex3 {
    public static void main(String[] args) {
        File imatge1 = new File("Ex3img/c1.jpg");
        File imatge2 = new File("Ex3img/c2.jpg");
        File imatge3 = new File("Ex3img/c3.jpg");
        hash(imatge1);
        hash(imatge2);
        hash(imatge3);
        System.out.println("\nEl hash 5d78088d6153c907682046c85387e9788d5f524f785e57986bba56e7493b34bb de l'enunciat correspon a la imatge c2.jpg, Las Meninas de Diego Velázquez");
    }

    public static void hash(File imatge){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA3-256");
            byte[] hashInBytes = new byte[0];
            hashInBytes = md.digest(Files.readAllBytes(imatge.toPath()));
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println(sb);
        } catch (Exception e) {
            e.printStackTrace(); }
    }
}
