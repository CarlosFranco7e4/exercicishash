package itb.cat.Ex1;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Ex1 {

    public static void main(String[] args) {
        String password = "M07";
        try {
            long createdMillis = System.currentTimeMillis();
            //MessageDigest md = MessageDigest.getInstance("SHA1");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //MessageDigest md = MessageDigest.getInstance("SHA3-256");
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            long nowMillis = System.currentTimeMillis();
            System.out.println(sb);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace(); }
    }
            /*
            Proveu amb SHA-2 (“SHA-256”) i SHA-3 (“SHA3-256”). Hi ha diferències?
            Depenent de quin SHA utilitzis, la contrasenya encriptada resultant serà més llarga o més curta.

            Quants bits té cada valor de hash?
            El SHA2 i el SHA3 tenen una mida de 256 bits.

            Quants caràcters hexadecimals?
            Són 64 caràcters per el SHA2 i SHA3.

            Quants segons trigueu en cada cas?
             */
}

